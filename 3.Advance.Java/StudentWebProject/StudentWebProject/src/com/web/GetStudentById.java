package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDao;
import com.dto.Student;


@WebServlet("/GetStudentById")
public class GetStudentById extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		int empId = Integer.parseInt(request.getParameter("stuId"));

		StudentDao studentDao = new StudentDao();
		int stuId;
		Student student = studentDao.getStudentById(stuId);

		out.print("<body bgcolor='lightyellow' text='green'>");	
		

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("HRHomePage");
		requestDispatcher.include(request, response);

		if (student != null) {
			out.print("<table border='2' align='center'>");

			out.print("<tr>");
			out.print("<th> StuId    </th>");
			out.print("<th> StuName  </th>");
			out.print("<th> Course   </th>");
			out.print("<th> Fees     </th>");
			out.print("<th> Gender   </th>");
			out.print("<th> Email-Id </th>");
			out.print("</tr>");

			out.print("<tr>");
			out.print("<td>" + student.getStuId()    + "</td>");
			out.print("<td>" + student.getStuName()  + "</td>");
			out.print("<td>" + student.getCourse()   + "</td>");
			out.print("<td>" + student.getFees()     + "</td>");
			out.print("<td>" + student.getGender()   + "</td>");
			out.print("<td>" + student.getEmailId()  + "</td>");
			out.print("</tr>");	

			out.print("</table>");
		} else {
			out.print("<h3 style='color:red;'>Unable to Fetch the Student Record</h3>");
		}

		out.print("</body>");

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
