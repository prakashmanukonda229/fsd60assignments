import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';

interface CartItem {
  name: string;
  price: number;
  }


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent {

  cartProducts: any;
  data: any;
  emailId: any;
  total: any;

  constructor() {
    this.total = 0;
    this.emailId = localStorage.getItem('emailId');
    this.cartProducts = [];
    this.data = localStorage.getItem('cartProducts');
    this.cartProducts = JSON.parse(this.data);

    this.cartProducts.forEach((element: any) => {
      this.total = this.total + element.price;
    });

  }

}
// export class CartComponent implements OnInit{
//   cartItems: CartItem[] = [];
//   totalAmount = 0;

//   constructor(private cartService: CartService) {}

//   ngOnInit() {
//     this.cartService.getCartItems().subscribe(items => {
//       this.cartItems = items;
//       this.calculateTotalAmount();
//     });
//   }

//   calculateTotalAmount() {
//     this.totalAmount = this.cartItems.reduce((total, item) => total + item.price, 0);
//   }

//   removeFromCart(index: number) {
//     this.cartService.removeItem(index);
//   }

//   purchaseItem(index: number) {
//     this.cartService.purchaseItem(index); // Call the service method to remove the item
//   }
// }



