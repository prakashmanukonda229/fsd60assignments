import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmpService {
  loginStatus: boolean;
  
  //Enable/Disable Login
  isUserLogged: any;

  constructor(private http: HttpClient) { 
    this.loginStatus = false;
    
    //Enable/Disable Login
    this.isUserLogged = new Subject();
  }
  getAllCountries():any{
    return this.http.get('https://restcountries.com/v3.1/all');
  }
  getAllEmployees(): any {
    return this.http.get('http://localhost:8085/getAllEmployees');
}
getEmployeeById(empId: any): any {
  return this.http.get('http://localhost:8085/getEmployeeById/' + empId);
}
getAllDepartments(): any {
  return  this.http.get('http://localhost:8085/getAllDepartments');
}
registerEmplooyee(emp: any) {
  return this.http.post('http://localhost:8085/addEmployee', emp);
}

getAllProducts(): any{
  return this.http.get('http://localhost:8085/getAllProducts');
}
  isUserLoggedIn(){
    this.loginStatus = true;
    
    //Enable/Disable Login
    this.isUserLogged.next(true);
  }
  isUserLoggedOut(){
    this.loginStatus = false;
     //Enable/Disable Login
     this.isUserLogged.next(false);
  }
  
  //Enable/Disable Login
  getIsUserLogged(): any {
    return this.isUserLogged.asObservable();
  }
  getLoginStatus():boolean{
    return this.loginStatus;
  }
  
  getAllCountries(): any {
    return this.http.get('https://restcountries.com/v3.1/all');
  }
  
updateEmployee(emp: any) {
    return this.http.put('http://localhost:8085/updateEmployee', emp);
}
deleteEmployee(empId:any){
  return this.http.delete('http://localhost:8085/deleteEmployeeById' +empId);
}
employeeLogin(emailId: any, password: any) {
  return this.http.get('http://localhost:8085/empLogin/' + emailId + '/' + password).toPromise();
}

  
}
